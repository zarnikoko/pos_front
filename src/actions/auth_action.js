import { store } from '..';
import {FetchMethod,host} from '../common/common';

export const SIGN_IN = (data) => {
    return store.dispatch({
        type : "SIGN_IN",
        ...data
    })
}

export const SIGN_IN_SUCCESS = (data) => {
    return store.dispatch({
        type : "SIGN_IN_SUCCESS",
        ...data
    })
}

export const SIGN_OUT_SUCCESS = () => {
    return store.dispatch({
        type : "SIGN_OUT_SUCCESS"
    })
}

export const SIGN_IN_FAIL = (data) => {
    return store.dispatch({
        type : "SIGN_IN_FAIL",
        ...data
    })
}

export const SIGN_OUT = () => {
    return store.dispatch({
        type : "SIGN_OUT",
    })
}

export const IS_SIGNNING_OUT = (status = true) => {
    return store.dispatch({
        type : "IS_SIGNNING_OUT",
        value : status
    })
}