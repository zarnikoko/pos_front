import { store } from "..";

export const SET_USER = (user) => {
    return store.dispatch({
        type : "SET_USER",
        value: user
    })
}

export const SET_HISTORY = (history) => {
    return store.dispatch({
        type : "SET_HISTORY",
        value : history
    })
}

export const SET_CURRENT_PLACE = (place) => {
    return store.dispatch({
        type : "SET_CURRENT_PLACE",
        value : place
    })
}