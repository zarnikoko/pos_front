import { all } from 'redux-saga/effects';
import { watchAuthChange } from './auth_saga';

export default function* rootSaga() {
   yield all([
      watchAuthChange(),
   ]);
}