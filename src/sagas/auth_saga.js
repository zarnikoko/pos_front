import {takeLatest,call,select} from 'redux-saga/effects';
import { SET_USER } from '../actions/user_action';
import { FetchMethod,host } from '../common/common';
import {SIGN_IN_SUCCESS,SIGN_OUT_SUCCESS,SIGN_IN_FAIL, IS_SIGNNING_OUT} from '../actions/auth_action'

const store_history  =  (state) => state.history;

function* sign_in(action){
    const {usernameInput,pwdInput,data,setLoading} = action;
    setLoading(true);
    try{
        const res = yield FetchMethod({
            url : host+"signin",
            sendData : data,
        });
        const rsp = yield res.json();
        const status = yield res.status;
        if(status===400){
            yield SIGN_IN_FAIL({rsp,usernameInput,pwdInput})
        }else if(status===202){
            yield SIGN_IN_SUCCESS(rsp);
            return;
        }
    }catch(error){
        yield SIGN_IN_FAIL({error})
    }
    setLoading(false);
}

function* sign_out(action){
    yield IS_SIGNNING_OUT(true)
    yield call(remove_refresh_token_in_db);
    yield IS_SIGNNING_OUT(false)
    yield SIGN_OUT_SUCCESS();
}

function* sign_in_success(action){
    const history = yield select(store_history);
    const {access_token,refresh_token,user} = action;
    yield localStorage.setItem("access_token", access_token);
    yield localStorage.setItem("refresh_token", refresh_token);
    yield SET_USER(user);
    yield history.push('/dashboard');
    // yield window.location.href = '/profile'
}

function* sign_out_success(){
    const history = yield select(store_history);
    yield localStorage.removeItem("access_token");
    yield localStorage.removeItem("refresh_token");
    yield SET_USER(null);
    yield history.push('/signin');
    // yield window.location.href = '/signin'
}

function* sign_in_fail(action){
    const {rsp,usernameInput,pwdInput,error} = yield action;
    if(error){
        yield alert(" Internal server error ! ")
    }else{
        yield alert(rsp.message);
        if(rsp.type!==undefined){
            if(rsp.type==='name'){
                yield usernameInput.current.select();
            }else if(rsp.type==='password'){
                yield pwdInput.current.select();
            }
        }
    }
}

function* remove_refresh_token_in_db(){
    try{
        yield FetchMethod({
            url : host+"signout",
            sendData : {refresh_token:localStorage.getItem('refresh_token')},
            isAuth:true,
            method : "delete"
        });
    }catch(error){
        console.log(error)
    }
}

export function* watchAuthChange(){
    yield takeLatest("SIGN_IN",sign_in);
    yield takeLatest("SIGN_IN_SUCCESS",sign_in_success);
    yield takeLatest("SIGN_IN_FAIL",sign_in_fail);
    yield takeLatest("SIGN_OUT",sign_out);
    yield takeLatest("SIGN_OUT_SUCCESS",sign_out_success);
}