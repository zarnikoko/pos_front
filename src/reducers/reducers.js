export const InitialState = {
    user : null,
    isSignningOut : false,
    history : null,
    currentPlace : null,
    success_message : null
}

export  const reducer = (state = InitialState, action) =>{
    const {type,value} =  action
    switch(type){
        case  'SET_USER' :
            return {...state, user : value};
        case 'IS_SIGNNING_OUT' :
            return {...state, isSignningOut : value} 
        case 'SET_HISTORY' :
            return {...state, history : value} 
        case 'SET_CURRENT_PLACE':
            return {...state, currentPlace : value} 
        default : 
            return state;
    }
}