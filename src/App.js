import React, { useEffect } from 'react';
import './App.css';
import {withRouter} from 'react-router-dom';
import Routes from './Components/Routes/Routes';

function App(props) {
    return (
        <Routes/>
    );
}

export default withRouter(App);
