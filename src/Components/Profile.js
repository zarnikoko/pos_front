import React from 'react';
import './NotFoundPage/NotFoundPage.css';
import { withRouter } from "react-router-dom";
import { SIGN_OUT } from '../actions/auth_action';

const Profile = (props) =>{
    return(
        <div className="flex-center position-ref full-height bg-warning">
            <div className="code">
                Me            </div>

            <div className="message" style={{padding:"10px"}}>
                And You?            </div>
        </div>
    )
}

export default withRouter(Profile)