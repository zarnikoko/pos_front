import React,{ useEffect, useRef,useState } from 'react';
import {Link,withRouter} from 'react-router-dom';
import './Signin.css';
import { SIGN_IN } from '../../actions/auth_action';

const Signin = (props)=>{
    const usernameInput = useRef(null);
    const pwdInput = useRef(null);
    const [loading,setLoading] = useState(false);
    const [data,setData] = useState({
        'username' : '',
        'password' : ''
    });

    const handleInputChange = (e) =>{
        e.persist();
        setData({
            ...data,
            [e.target.name] : e.target.value
        })
    }

    const handleSignin = async(e) => {
        e.preventDefault();
        SIGN_IN({data,usernameInput,pwdInput,setLoading})
    }

    return(
        <div className="d-flex align-items-center flex-column justify-content-center h-100 bg-dark text-white login-main" id="header">
            <div className="login-container">
                {/* <center> */}
                    {/* <div className="middle"> */}
                        <div id="login">
                            <form noValidate onSubmit={e=>handleSignin(e)}>
                                {/* <fieldset className="clearfix"> */}
                                    <p>
                                        <span className="fa fa-user"></span>
                                        <input 
                                        type="text"  
                                        placeholder="User Name" 
                                        required 
                                        name="username"
                                        ref={usernameInput}
                                        value={data.username}
                                        onChange={(e)=>handleInputChange(e)}/>
                                    </p>
                                    <p>
                                        <span className="fa fa-lock"></span>
                                        <input 
                                        type="password"  
                                        placeholder="Password" 
                                        required
                                        name="password"
                                        ref={pwdInput}
                                        value={data.pwd}
                                        onChange={(e)=>handleInputChange(e)}/>
                                    </p>
                                    <div>
                                        <span style={{width:"48%",textAlign:"left",display:"inline-block"}}><Link className="small-text" to="/signin/forgot password">Forgot
                                        password?</Link></span>
                                        <span style={{width:"50%",textAlign:"right",display:"inline-block"}}>
                                            <button
                                                disabled={loading}
                                            > 
                                                {loading?<span><span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Signing...</span>
                                                :
                                                  <span>Sign in</span>
                                                } 
                                            </button>
                                        </span>
                                    </div>
                                {/* </fieldset> */}
                                {/* <div className="clearfix"></div> */}
                            </form>
                            {/* <div className="clearfix"></div> */}
                        </div>
                        {/* <div className="logo">
                            LOGO
                            <div className="clearfix"></div>
                        </div> */}
                    {/* </div> */}
                {/* </center> */}
            </div>
        </div>
    )
}

export default withRouter(Signin)