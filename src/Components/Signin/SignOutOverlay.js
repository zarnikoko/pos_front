import React from 'react'
import { useSelector } from 'react-redux'

const SignOutOverlay = (props) => {
    const isSignOut = useSelector(state=>state.isSignningOut);
    if(isSignOut){
        return <div style={{
            position:"fixed",
            top:"0",
            left:"0",
            height:"100vh",
            width:"100vw",
            background:"rgba(255,255,255,0.6)",
            zIndex:"2000",
        }}>
            <div className="d-flex align-items-center flex-column justify-content-center vh-100">
                <div className="bg-dark p-2" style={{color:"white"}}>
                    <span 
                        className="spinner-border spinner-border-md" 
                        role="status" 
                        aria-hidden="true"
                    ></span><span style={{fontSize:"1.5em"}} className="ml-2">Signning Out ... </span>
                </div>
            </div>
        </div>
    }
    return <></>
}

export default SignOutOverlay