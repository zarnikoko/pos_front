import React,{ useEffect, useRef, useState }from 'react'
import { withRouter } from 'react-router-dom'
import { AsyncBtn, FetchMethod, host } from '../../common/common'

const NewUser = (props) => {
    const {history} = props
    const {state} = history.location
    const {username='',psw='',conPsw='',rID=''} = state || {}
    const [pLoading,setPLoading] = useState(false)
    const [cLoading,setCLoading] = useState(false)
    const [name,setName] = useState(username)
    const [password,setPassword] = useState(psw)
    const [comfirmPassword,setComfirmPassword] = useState(conPsw)
    const [role_id,setRoleID] = useState(rID)
    const [roles,setRoles] = useState([])
    const nameRef = useRef(null)
    const passwordRef = useRef(null)
    const comfirmPasswordRef = useRef(null)

    const handleSubmit = async (e) => {
        e.preventDefault()
        setCLoading(true)
        try{
            const res = await FetchMethod({
                url : `${host}users/store`,
                sendData : {username:name,password:password,role_id:role_id},
                isAuth:true
            })
            const status = await res.status;
            if(status === 202){
                const rsp = await res.json()
                reset()
            }
        }catch(err){
            console.log(err)
        }
        setCLoading(false)
    }

    const loadRoles = async() => {
        setPLoading(true)
        try{
            const res = await FetchMethod({
                url : `${host}roles`,
                method : 'GET',
                isAuth : true
            })
            const status =   await res.status
            if(status===200){
                const rsp = await res.json()
                setRoles(rsp.roles)
            }
        }catch(err){
            console.log(err)
        }
        setPLoading(false)
    }

    const reset = () => {
        setName('')
        setRoleID('')
        setPassword('')
        setComfirmPassword('')
    }


    useEffect(()=>{
        loadRoles()
        console.log(history)
    },[])

    return(
        <div className="bg-light text-dark p-5">
            <form noValidate onSubmit={e => handleSubmit(e)}>
                <div className="form-group row">
                    <label 
                    htmlFor="name" 
                    className="col-sm-3 col-form-label"
                    >
                        User Name
                    </label>
                    <div className="col-sm-5">
                        <input 
                            type="text" 
                            className="form-control" 
                            name="name"
                            placeholder="User Name"
                            autoComplete="Off"
                            value={name}
                            autoFocus={true}
                            ref={nameRef}
                            onChange = {(e)=>setName(e.target.value)}
                        />
                        <span className="text-danger"></span>
                    </div>
                </div>
                <div className="form-group row">
                    <label 
                    htmlFor="role" 
                    className="col-sm-3 col-form-label"
                    >
                        Role
                    </label>
                    <div className="col-sm-5">
                        <select
                            className="form-control col-lg-8 col-md-8 col-sm-8 col-8"
                            onChange = {(e) =>  setRoleID(e.target.value)}
                            value={role_id}
                            style={{display:'inline-block',verticalAlign:'middle'}}
                        >
                            <option value="">Please select a role</option>
                            {roles.map((role,i)=>{
                                return <option key={i} value={`${role._id}`}>{role.name}</option>
                            })}
                        </select>
                        <button 
                            className={'btn btn-primary btn-sm ml-2'}
                            onClick = {()=> history.push('/create-role',{username:name,psw:password,conPsw:comfirmPassword,rID:role_id})}
                        >
                            New
                        </button>
                        <span className="text-danger"></span>
                    </div>
                </div>
                <div className="form-group row">
                    <label 
                    htmlFor="password" 
                    className="col-sm-3 col-form-label"
                    >
                        Password
                    </label>
                    <div className="col-sm-5">
                        <input 
                            type="password" 
                            className="form-control" 
                            name="password"
                            placeholder="Password"
                            autoComplete="Off"
                            value={password}
                            ref={passwordRef}
                            onChange = {(e)=>setPassword(e.target.value)}
                        />
                        <span className="text-danger"></span>
                    </div>
                </div>
                <div className="form-group row">
                    <label 
                    htmlFor="comfirm-password" 
                    className="col-sm-3 col-form-label"
                    >
                        Comfirm Password
                    </label>
                    <div className="col-sm-5">
                        <input 
                            type="password" 
                            className="form-control" 
                            name="comfirm-password"
                            placeholder="Comfirm Password"
                            autoComplete="Off"
                            value={comfirmPassword}
                            ref={comfirmPasswordRef}
                            onChange = {(e)=>setComfirmPassword(e.target.value)}
                        />
                        <span className="text-danger"></span>
                    </div>
                </div>
                <div className="form-group row">
                    <div className="col-sm-12">
                        <AsyncBtn
                            pLoading = {pLoading}
                            cLoading = {cLoading}
                            pText = 'Please wait...'
                            cText = 'Creating...'
                            cNormalText = 'Create'
                            className = 'btn btn-sm btn-primary col-md-4 col-xl-2 col-12 col-sm-6 col-form-label mt-5'
                        />
                    </div>
                </div>
            </form>
        </div>
    )
}

export default withRouter(NewUser)