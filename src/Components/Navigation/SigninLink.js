import React from 'react'
import { useSelector } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'


const SigninLink = (props) => {
    const auth = useSelector(state => state.auth);
    if(auth === false){
        return <li className="nav-item ml-auto">
            <button type="button" id="sidebarCollapse" className="ml-lg-2 mt-2 mt-lg-0 mt-md-2 mt-sm-2 btn btn-outline-primary" onClick={()=>props.history.push('/signin')}>
                <i className="fa fa-sign-in" aria-hidden="true"></i><span> Sign in </span>
            </button>
        </li>
    }
    return (<></>)
}

export default  withRouter(SigninLink)