// navbar approximate height 54px 
import React from 'react'
import { useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom'
import SigninLink from './SigninLink';
import UserDropDown from './UserDropDown';

const sb_status = localStorage.getItem('sidebar')

export const atv = (sb_status === 'closed') ? 'active' : ''

const NavbarComponent = (props)=>{
    const place = useSelector(state=>state.currentPlace);
    const handleSideBarToggle = ()=>{
        let sb =  localStorage.getItem('sidebar');
        if(sb){
            if(sb==='opened'){
                localStorage.setItem('sidebar','closed')
            }else{
                localStorage.setItem('sidebar','opened')
            }
        }else{
            localStorage.setItem('sidebar','closed')
        }
        var sidebar = document.getElementById("sidebar");
        var navbar = document.getElementById("navbar");
        var compo = document.getElementById("component-container");
        sidebar.classList.toggle("active");
        navbar.classList.toggle("active");
        compo.classList.toggle("active");
    }
    return(
        <nav className={`navbar navbar-expand-lg navbar-light bg-light fixed-top  ${atv}`} id="navbar">
            <div className="container-fluid">
                <button type="button" id="sidebarCollapse" className="btn btn-info" onClick={()=>handleSideBarToggle()}>
                    <i className="fa fa-list" aria-hidden="true"></i><span> Menu </span>
                </button>
                <button className="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i className="fa fa-list" aria-hidden="true"></i>
                </button>
                <h4 className="ml-3 text-info">{place}</h4>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="nav navbar-nav ml-auto">
                        <UserDropDown/>
                        <SigninLink/>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default withRouter(NavbarComponent)