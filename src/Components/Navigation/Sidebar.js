import React,{useEffect, useRef} from "react";
import {withRouter,Link} from 'react-router-dom';
import SidebarComponent from './SidebarComponent';
import NavbarComponent from './NavbarComponent';
import './Sidebar.css';
import {atv} from './NavbarComponent'
const Sidebar = props => {
    return (
        <div className="wrapper">
            <SidebarComponent/>
            <div id="content">
                <NavbarComponent/>
                <div id='component-container' className={` ${atv}`}>
                    {props.children}
                </div>
            </div>
        </div>
    );
  };

export default withRouter(Sidebar)