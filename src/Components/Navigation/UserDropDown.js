import React from 'react'
import { useSelector } from 'react-redux';
import def_user_avator from '../../images/users_avator/default.png';
import { withRouter,Link } from 'react-router-dom'
// import { signout } from '../../common/common';
import { SIGN_OUT } from '../../actions/auth_action';
const UserDropDown = (props)=>{
    const user = useSelector(state=>state.user);
    if(user){
        return <li className="nav-item dropdown">
                <a className="nav-link btn btn-outline-secondary mt-2 mt-lg-0 mt-md-2 mt-sm-2" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{margin:"0 2px"}}>
                    <img width="25" height="23" className="" src={def_user_avator} /> <i className="fa fa-angle-down" aria-hidden="true"></i>
                </a>
                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a className="dropdown-item" href="#">{user.name}</a>
                <Link className="dropdown-item" to="/profile">Profile</Link>
                <div className="dropdown-divider"></div>
                <a className="dropdown-item" href="#" onClick={()=>SIGN_OUT()}>Sign out</a>
            </div>
        </li>
    }
    return <></>
}

export default withRouter(UserDropDown)