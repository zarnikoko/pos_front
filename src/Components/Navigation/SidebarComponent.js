import React from 'react';
import { useSelector } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import './Sidebar.css';
import SidebarLinks from './SidebarLinks';
import {atv} from './NavbarComponent'
const SidebarComponent = (props) => {
    return(
        <nav id="sidebar" className={` ${atv}`}>
            <div className="sidebar-header">
                <h3>POS For Shop</h3>
            </div>
            <ul className="list-unstyled components">
                <p>Dummy Heading</p>
                <SidebarLinks/>
            </ul>
            <ul className="list-unstyled CTAs">
                <li>
                    <a>© 2021 POS For Shops.<br/>All Rights Reserved</a>
                </li>
                <li>
                    <a>Version 1.0.0.0 </a>
                </li>
            </ul>
        </nav>
    )
}

export default withRouter(SidebarComponent);