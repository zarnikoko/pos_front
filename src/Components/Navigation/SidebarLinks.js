import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

export const defaultLinks = ['home','profile'];

const SidebarLinks = (props) => {
    const user = useSelector(state=>state.user);
    const {permissions=[]} = user || {};
    const link_builder = permissions.filter((v,i)=>v.sidebar).map((p,indx)=>{
        // if(p.path!==''){
        //     return <li key={'parent-link'+indx}>
        //         <Link to={p.path}>{p.display}</Link>
        //     </li>
        // }else{
        //     return <li key={'parent-link'+indx}>
        //         <a href={'#'+p.name+'Submenu'} data-toggle="collapse" aria-expanded="false" className="dropdown-toggle">{p.display}</a>
        //         <ul className="collapse list-unstyled" id={p.name+'Submenu'}>
        //             {p.childs.map((c_p,i)=>{
        //                 return <li key={'child-link-'+i}>
        //                     <Link to={c_p.path}>{c_p.display}</Link>
        //                 </li>
        //             })}
        //         </ul>
        //     </li>
        // }
        if(p.path!=='' && p.parent === ''){
            return <li key={'parent-link'+indx}>
                <Link to={p.path}>{p.display}</Link>
            </li>
        }else if(p.path===''){
            return <li key={'parent-link'+indx}>
                <a href={'#'+p.name+'Submenu'} data-toggle="collapse" aria-expanded="false" className="dropdown-toggle">{p.display}</a>
                <ul className="collapse list-unstyled" id={p.name+'Submenu'}>
                    {permissions.filter((v,i)=>v.parent===p.name && v.sidebar).map((c_p,i)=>{
                        return <li key={'child-link-'+i}>
                            <Link to={c_p.path}>{c_p.display}</Link>
                        </li>
                    })}
                </ul>
            </li>
        }else{
            return null
        }
    })

    return(
        <>
            {link_builder}
        </>
    )
}

export default SidebarLinks
