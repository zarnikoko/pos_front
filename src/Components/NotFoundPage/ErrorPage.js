import React from 'react';
import './NotFoundPage.css';
const ErrorPage = () =>{
    return(
        <div className="flex-center position-ref full-height">
            <div className="code">
                ERR_DISCONNECTED_SERVER :(           
            </div>
            <div className="message" style={{padding:"10px"}}>
                Refresh again later
            </div>
        </div>
    )
}

export default ErrorPage