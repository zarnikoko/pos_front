
import Dashboard from '../Dashboard/Dashboard'
import Signin from '../Signin/Signin'
import Profile from '../Profile'
import Page1 from '../Page1'
import Roles from '../Roles/Roles'
import NewRole from '../Roles/NewRole'
import Users from '../Users/Users'
import NewUser from '../Users/NewUser'
const routes = [
    {
        path:'/',
        name:'dashboard',
        display:'Home',
        exact:true,
        component:()=><Dashboard/>,
        auth:'private',
    },
    {
        path:'/dashboard',
        name:'dashboard',
        display:'Dashboard',
        exact:true,
        component:()=><Dashboard/>,
        auth:'private'
    },
    {
        path:'/signin',
        exact:true,
        component:()=><Signin/>,
        auth:'public',
        retricted : true
    },
    {
        path:'/profile',
        name:'profile',
        display:'Profile',
        exact:true,
        component:()=><Profile/>,
        auth:'private'
    },
    {
        path:'/page1',
        name:'page1',
        display:'Page 1',
        exact:true,
        component:()=><Page1/>,
        auth:'private'
    },
    {
        path:'/roles',
        name:'roles',
        display:'Roles',
        exact:true,
        component:()=><Roles/>,
        auth:'private'
    },
    {
        path:'/create-role',
        name:'create-role',
        display:'New Role',
        exact:true,
        component:()=><NewRole/>,
        auth:'private'
    },
    {
        path:'/edit-role',
        name:'roles',
        display:'Edit Role',
        exact:true,
        component:()=><NewRole/>,
        auth:'private'
    },
    {
        path : '/users',
        name : 'users',
        display : 'Users',
        exact : true,
        component : () => <Users/>,
        auth : 'private'
    },
    {
        path : '/create-user',
        name : 'create-user',
        display : 'New User',
        exact : true,
        component : () => <NewUser/>,
        auth : 'private'
    }
]

export default routes;