import React,{useEffect,useState,useRef, useContext, useCallback} from 'react';
import { useSelector } from 'react-redux';
import { Route, Redirect,withRouter, useHistory } from 'react-router-dom';
import { SIGN_OUT_SUCCESS } from '../../actions/auth_action';
import { SET_CURRENT_PLACE, SET_HISTORY, SET_SUCCESS_MESSAGE, SET_USER } from '../../actions/user_action';
import { FetchMethod, host } from '../../common/common';
import ErrorPage from '../NotFoundPage/ErrorPage';
import LoadingShowPage from '../NotFoundPage/LoadingShowPage';

// export const CheckPermission = (route_name,permissions=[]) => {
//   let permissions_name = []
//   permissions.forEach((p)=>{
//     if(p.path !== ''){
//       permissions_name.push(p.name)
//     }else{
//       p.childs.forEach((cp)=>{
//         permissions_name.push(cp.name)
//       })
//     }
//   });
//   if(permissions_name.indexOf(route_name)>=0){
//     return true
//   }
//   return false;
// }

export const checkPermission = (route_name,permissions=[]) => {
  const result = permissions.filter((v,i)=>v.name===route_name).length>0?true:false;
  return result;
}

export const useCheckLogin = () => {
  const accessToken = localStorage.getItem('access_token');
  if(accessToken){
    return true
  }
  return false
}

const Public_Route = ({component: Component, restricted, ...rest}) => {
  const checkLogin = useCheckLogin();
  const history = useHistory();
  SET_HISTORY(history);
  useEffect(()=>{
    SET_CURRENT_PLACE(null);
  },[])
  return (
      <Route {...rest} render={props => (
        checkLogin && restricted ?
          <Redirect to="/dashboard" />
          : <Component {...props} key={Component} />
      )} />
  );
};

const Private_Route = ({component: Component,name, ...rest}) => {
  const history = useHistory();
  const user = useSelector(state=>state.user);
  const checkLogin = useCheckLogin();
  const [Compo, setComponent] = useState(()=>{
    if(checkLogin && user===null){
      return <LoadingShowPage key={'loadingPage'}/> 
    }else{
      if(checkLogin){
        if(checkPermission(name,user.permissions)){
          return <Component  key={Component}/>
        }
        return <Redirect to="/dashboard" key={Component}/>
      }
      return <Redirect to="/signin"/>
    }
  })

  // const signout = () => {
  //   SIGN_OUT_SUCCESS()
  //   setComponent(<Redirect to="/signin" key={Component}/>)
  //   history.push('/signin')
  // }

  useEffect(()=>{
    async function userinfo() {
      if(checkLogin && user===null){
        try{
          const genUserInfo = await FetchMethod({
            url : host+"check_token",
            isAuth:true
          });
          const status = await genUserInfo.status;
          if(status===202){
            const res = await genUserInfo.json();
            SET_USER(res.user);
            if(checkPermission(name,res.user.permissions)) {
              setComponent(<Component  key={Component}/>)
            }else{
              setComponent(<Redirect to="/dashboard" key={Component}/>)
            }
          }else{
            SIGN_OUT_SUCCESS()
          }
        }catch(error){
          setComponent(<ErrorPage  key={'errorPage'}/>)
          console.log(error)
          // SIGN_OUT_SUCCESS()
        }
      }
    }
    SET_HISTORY(history);
    SET_CURRENT_PLACE(rest.display);
    userinfo()
  },[])

  return (
    <Route {...rest} render={props => (
      Compo
    )} />
  );
};
export const PrivateRoute = withRouter(Private_Route);
export const PublicRoute = withRouter(Public_Route);

