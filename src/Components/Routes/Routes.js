import React from 'react'
import {BrowserRouter as Router,Route,Redirect,Switch} from 'react-router-dom';
import Sidebar from '../Navigation/Sidebar';
import {PublicRoute,PrivateRoute} from './routes_authorization';
import routes from './routes_path';
import SignOutOverlay from '../Signin/SignOutOverlay'
const Routes = (props) => {
    const route_components = routes.map((route,index)=>{
        if(route.auth==='private'){
            return <PrivateRoute key={index} component={route.component} path={route.path} exact={route.exact} name={route.name} display={route.display}/>
        }
        
        return <PublicRoute  key={index} restricted ={route.retricted} component={route.component} path={route.path} exact={route.exact}/>
        
    })
    return(
        <Router>
            <Route render={({ location, history }) => (
                <React.Fragment>
                    <SignOutOverlay/>
                    <Sidebar>
                        <Switch>
                            {route_components}
                            <Redirect to="/dashboard"/>
                        </Switch>
                    </Sidebar>
                </React.Fragment>
            )}/>      
        </Router>
    )
}

export default Routes