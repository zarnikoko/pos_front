import React from "react";
import { withRouter } from "react-router-dom";
import '../Navigation/Sidebar.css';

const Dashboard = props => {
    return (
        <div className="d-flex align-items-center flex-column justify-content-center h-100 bg-dark text-white" id="header">
            <h1 className="display-4">Hello.</h1>
            <form>
                <div className="form-group">
                    <input className="form-control form-control-lg" placeholder="Email" type="text"/>
                </div>
                <div className="form-group">
                    <input className="form-control form-control-lg" placeholder="Password" type="text"/>
                </div>
                <div className="form-group">
                    <button className="btn btn-info btn-lg btn-block">Sign In</button>
                </div>
            </form>
        </div>
    );
  };

  export default withRouter(Dashboard)