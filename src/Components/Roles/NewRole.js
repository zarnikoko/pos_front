import React,{ useEffect, useRef, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { AsyncBtn, FetchMethod, host, useAlertSuccessDiv, useConsoleLog } from '../../common/common';

const NewRole = (props) => {
    const {history} =  props;
    const {pathname,state} =  history.location;
    const {role_id=null} = state || {}
    const [pLoading,setPLoading] = useState(false)
    const [cLoading,setCLoading] = useState(false)
    const [rolename,setRoleName] = useState('')
    const [permissions,setPermissions] = useState([])
    const [rolePermissions,setRolePermissions] = useState([])
    const [msg,setMsg] = useState(null);
    const [err,setErr] =  useState({});
    const roleNameRef = useRef(null);
    const successAlert = useAlertSuccessDiv(msg);
    const _mounted = useRef(false)
    const loadPermission = async () => {
        setPLoading(true);
        try{
            const res = await FetchMethod({
                url : host+"permissions",
                isAuth:true,
                method : "GET"
            });
            const status = await res.status
            if(status===200){
                const rsp = await res.json()
                setPermissions(rsp.permissions)
                if(role_id) loadEditRole()
            }
        }catch(error){
            window.location.reload();
            console.log(error);
            return
        }   
        setPLoading(false);
    }
    const loadEditRole =  async () => {
        try{
            const res = await FetchMethod({
                url : host+`roles/${role_id}/edit`,
                isAuth:true,
                method : "GET"
            });
            const status = await res.status
            if(status===200){
                const rsp = await res.json()
                setRoleName(rsp.role.name)
                setRolePermissions(rsp.role.permissions)
            }
        }catch(error){
            // window.location.reload()
            console.log(error)
            return
        }   
    }
    const handleChkboxChanged = (e,p) => {
        const exist = isRolePermissionExist(p);
        if(e.target.checked){
            if(!exist){
                let concated = [p];
                if(rolePermissions.some((rps)=>{return rps.name === p.parent})){
                    concated = concated.concat(permissions.filter((pm,index)=>pm.parent === p.name))
                }else{
                    concated = concated.concat(permissions.filter((pm,index)=>pm.parent === p.name || pm.name === p.parent))
                }
                setRolePermissions(rolePermissions.concat(concated))
            }
        }else{
            if(exist){
                let removedRP = rolePermissions;
                if(rolePermissions.filter((rP)=>rP.parent === p.parent).length>1){
                    removedRP = rolePermissions.filter((rp)=>rp._id !== p._id && rp.parent !== p.name ); 
                }else{
                    removedRP =  rolePermissions.filter((rp)=>rp._id !== p._id && rp.parent !== p.name && rp.name !== p.parent )
                }
                setRolePermissions(removedRP);
            }
        }
    }

    const handleAllChecked = (e) => {
        if(e.target.checked){
            setRolePermissions(permissions)
        }else{
            setRolePermissions([])
        }
    }

    const isRolePermissionExist = (p) => {
        const result = rolePermissions.some((rp)=>{
            return rp._id === p._id
        })
        return result;
    }

    const handleSubmit = async(e) => {
        e.preventDefault();
        setCLoading(true)
        try{
            let storeOrUpdate = {
                isAuth : true,
                sendData : {name:rolename,permissions:rolePermissions}
            }
            if(role_id){
                storeOrUpdate.url = host+`roles/${role_id}`
                storeOrUpdate.method = 'patch'
            }else{
                storeOrUpdate.url = host+'roles'
            }
            const res = await FetchMethod(storeOrUpdate)
            const status = await res.status;
            const rsp = await res.json()
            console.log(rsp)
            if(status === 201){
                setErr({})
                setRoleName('')
                setRolePermissions([])
                setMsg("New role is added successfully")
                roleNameRef.current.select()
            }else if(status === 400){
                console.log(rsp)
                if(rsp.field === "Role's Name") roleNameRef.current.select()
                setErr({
                    [rsp.field] : rsp.message
                })
            }else if(status === 200){
                setMsg("Role is updated successfully")
            }
        }catch(error){
            console.log(error)
        }
        setCLoading(false)
    }

    const removeMessage = () => {
        setMsg(null)
    }

    useEffect(()=>{
        if(pathname==='/edit-role' && role_id===null){
            history.push('/dashboard')
            return
        }
        loadPermission();
        document.body.addEventListener('click',removeMessage)
        document.body.addEventListener('keydown',removeMessage)
        return() => {
            document.body.removeEventListener('click',removeMessage)
            document.body.removeEventListener('keydown',removeMessage)
        }
    },[])

    useEffect(()=>{
        if(!_mounted.current){
            _mounted.current = true
        }else{
            console.log(permissions)
        }
    },[permissions])

    // useConsoleLog(rolePermissions)

    return(
        <>
             {(state!==undefined && state.role_id === undefined)?
                <button 
                    className="btn btn-info btn-sm text-white pl-5 pr-5 mt-3 ml-1"
                    onClick={()=>history.push('/create-user',state)}
                >
                    Continues to New User
                </button>
            :
                null
            }
            <div className="bg-light text-dark p-5">
                {successAlert}
                <form noValidate onSubmit={e => handleSubmit(e)}>
                    <div className="form-group row">
                        <label 
                        htmlFor="rolename" 
                        className="col-sm-2 col-form-label"
                        style={{textAlign:'center'}}
                        >
                            Role's Name
                        </label>
                        <div className="col-sm-5">
                            <input 
                                type="text" 
                                className="form-control" 
                                name="rolename"
                                placeholder="Role's Name"
                                value={rolename}
                                autoFocus={true}
                                ref={roleNameRef}
                                onChange = {(e)=>setRoleName(e.target.value)}
                            />
                            <span className="text-danger">{err["Role's Name"]}</span>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label 
                        htmlFor="permissions" 
                        className="col-sm-2 col-form-label"
                        style={{textAlign:'center'}}
                        >
                            Permissions
                        </label>
                        <div className="col-sm-5">
                            {
                                (permissions.length>0)?
                                    <div className="mt-2">
                                        <ul>
                                            <li type="square">
                                                <input 
                                                    type="checkbox"
                                                    onChange={(e)=>handleAllChecked(e)}
                                                    checked={ permissions.length === rolePermissions.length }
                                                    className="mr-2 mt-2"
                                                />{'All'}
                                            </li>
                                            {permissions.filter((p,idx)=>p.parent==='').map((v,i)=>{
                                                return <React.Fragment key={'permission'+i}>
                                                    <li type="square">
                                                        <input 
                                                            type="checkbox"
                                                            onChange={(e)=>handleChkboxChanged(e,v)}
                                                            checked={isRolePermissionExist(v)}
                                                            className="mr-2 mt-2"
                                                        />{v.display}
                                                    </li>
                                                    {permissions.filter((ch,j)=>ch.parent===v.name).map((vch,k)=>{
                                                    return <li key={'vch'+k} className="ml-3">
                                                            <input 
                                                                type="checkbox"
                                                                onChange={(e)=>handleChkboxChanged(e,vch)}
                                                                checked={isRolePermissionExist(vch)}
                                                                className="mr-2 mt-2"
                                                            />{vch.display}
                                                        </li>
                                                    })}
                                                </React.Fragment>
                                            })}
                                        </ul>
                                    </div>
                                :null
                            }
                            <span className="text-danger">{err["Permissions"]}</span>
                        </div>
                    </div>
                    <div className="form-group row">
                        <div className="col-sm-12">
                            {
                                (role_id)?
                                    <AsyncBtn
                                        pLoading = {pLoading}
                                        cLoading = {cLoading}
                                        pText = 'Please wait...'
                                        cText = 'Updating...'
                                        cNormalText = 'Update'
                                        className = 'btn btn-sm btn-primary col-md-4 col-xl-2 col-12 col-sm-6 col-form-label'
                                    />
                                :
                                    <AsyncBtn
                                    pLoading = {pLoading}
                                    cLoading = {cLoading}
                                    pText = 'Please wait...'
                                    cText = 'Creating...'
                                    cNormalText = 'Create'
                                    className = 'btn btn-sm btn-primary col-md-4 col-xl-2 col-12 col-sm-6 col-form-label'
                                    />
                            }
                        </div>
                    </div>
                </form>
            </div>
        </>
    )
}

export default withRouter(NewRole)

