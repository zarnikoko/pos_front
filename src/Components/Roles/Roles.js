import React, { useState , useEffect, useRef } from 'react'
import { useHistory, withRouter } from 'react-router-dom'
import { AsyncBtn, FetchMethod, host ,SortColumn } from '../../common/common'
import LoadingShowPage from '../NotFoundPage/LoadingShowPage'

const Roles = (props) => {
    const [isLoad,setIsLoad] =  useState(false)
    const [cLoad,setCLoad] = useState({})
    const [isDeleting,setIsDeleting] = useState(false)
    const [name,setName] = useState('')
    const [roles,setRoles] = useState([])
    const [sortBy,setSortBy] = useState('updated_at')
    const [sortType,setSortType] = useState('desc')
    const history = useHistory()
    const _mounted = useRef(false)
    const loadRoles =  async () => {
        setIsLoad(true)
        try{
            const res = await  FetchMethod({
                url : host+`roles?sortBy=${sortBy}&sortType=${sortType}&name=${name}`,
                method : 'get',
                isAuth : true
            })
            const status = await res.status
            const rsp = await res.json()
            if(status === 200){
                setRoles(rsp.roles)
            }
        }catch(err){
            console.log(err)
            // window.location.reload()
        }
        setIsLoad(false)
    }

    const rolesRow = roles.map((role,i)=>{
        return <tr key={i} className="row">
            <td className="col">
                <button 
                    className="btn btn-link"
                    onClick={()=>{
                       alert('comming soon')
                    }}
                >
                    {role.name}
                </button><br/>
                <span style={{fontSize:"0.1em"}}>Updated : </span> <br/>
                <span style={{fontSize:"0.1em"}}>Created : </span>
            </td>
            <td className="text-right col-3">{role.countOfPermissions}</td>
            <td className="text-left col">
                <AsyncBtn
                    forcedDisabled = {isDeleting}
                    cNormalText = 'Edit'
                    className = 'btn btn-primary btn-sm'
                    onClick ={()=>history.push('/edit-role',{role_id:role._id})}
                />
                {/* <button className="btn btn-danger btn-sm" onClick={()=>destroyRole(role._id)}>Delete</button> */}
                <AsyncBtn
                    cLoading = {cLoad[role._id]}
                    forcedDisabled = {isDeleting}
                    cText = 'Del...'
                    cNormalText = 'Delete'
                    className = 'btn btn-danger btn-sm ml-2'
                    onClick = {()=>destroyRole(role._id)}
                />
            </td>
        </tr>
    })

    const destroyRole = async (id) => {
        if(window.confirm('Are you sure to delete this role?')){
            setCLoad({
            [id] : true
            })
            setIsDeleting(true)
            try{
                const res = await  FetchMethod({
                    url : host+`roles/${id}`,
                    method : 'delete',
                    isAuth : true
                })
                const status = await res.status
                const rsp = await res.json()
                if(status === 200){
                    console.log(rsp)
                    setRoles(roles.filter((role)=>role._id!==rsp.deleted_id))
                }
            }catch(err){
                console.log(err)
                window.location.reload()
            }
            setCLoad({
                [id] : false
            })
            setIsDeleting(false)
        }
    }

    const handleSort = (by,type) => {
        setSortBy(by)
        setSortType(type)
    }

    useEffect(()=>{
        loadRoles()
    },[])

    useEffect(()=>{
        if(!_mounted.current){
            _mounted.current = true
        }else{
            const timeout = setTimeout(() => {
                loadRoles()
            },500)
            return () => {
                clearTimeout(timeout);
            } 
        }
    },[sortBy,sortType,name])

    return(
        <div className="bg-light text-dark p-3">
            <LoadingShowPage
            show={isDeleting}
            text = 'Deleting Role...'
            />
            <div className="mb-5">
                <button 
                className="btn btn-outline-primary float-right"
                onClick={()=>{
                    history.push('/create-role') 
                    return
                }}
                >   New Role
                </button>
            </div>
            <div className="container pb-5">
                <input 
                    type="text"
                    value={name}
                    placeholder="Search Role..."
                    className={'form-control col-6 col-sm-6 col-md-6 col-lg-3 mb-3'}
                    onChange = {(e)=>{setName(e.target.value)}}
                />
                <table className="table table-hover">
                    <thead>
                        <tr className="row bg-primary text-white">
                            <th className="text-center col">
                                <SortColumn 
                                    columnLabel = "Role's Name"
                                    column = "name"
                                    sortBy = {sortBy}
                                    sortType = {sortType}
                                    data = {roles}
                                    handleSort = {handleSort}
                                />
                            </th>
                            <th className="text-center col-3">
                                <SortColumn 
                                    columnLabel = "Permission Count"
                                    column = "countOfPermissions"
                                    sortBy = {sortBy}
                                    sortType = {sortType}
                                    data = {roles}
                                    handleSort = {handleSort}
                                />
                            </th>
                            <th className="text-center col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {(isLoad)?
                        <tr style={{height:"100%"}}>
                            <td colSpan="3" align="center" style={{verticalAlign:"middle"}}>
                                <div className="spinner-border text-primary" role="status">
                                    <span className="sr-only">Loading...</span>
                                </div>
                                <div><h6>&nbsp;&nbsp;&nbsp;Loading ... </h6></div>
                            </td>
                        </tr>
                        :(roles.length>0)? rolesRow
                        :
                        <tr style={{height:"100%"}}>
                            <td colSpan="4" align="center" style={{verticalAlign:"middle"}}>
                                <span className="text-muted">No roles have been found .</span>
                            </td>
                        </tr>
                        }
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default withRouter(Roles)

