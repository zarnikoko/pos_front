import { useEffect } from 'react'
import { SET_SUCCESS_MESSAGE } from '../actions/user_action';

export const host = "http://"+window.location.hostname+process.env.REACT_APP_API_PORT+"api/"

export const FetchMethod = async (params) => {
    
   params.url = (params.url===undefined)?"":params.url;
   params.sendData = (params.sendData===undefined)?{}:params.sendData;
   params.isAuth = (params.isAuth===undefined)?false:params.isAuth;
   params.method = (params.method===undefined)?"POST":params.method.toUpperCase();
   params.headers = (params.headers===undefined)?{}:params.headers;

   const {url,sendData,isAuth,method,headers} = params;
   let default_headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
   };
   
   if(isAuth){
      default_headers.Access_Token = localStorage.getItem('access_token');
   }
   if(Object.keys(headers).length !== 0 && headers.constructor === Object){
      for (var key in headers) {
         default_headers[key] = headers[key];
      }
   }
   let default_menu = {
      method : method,
      headers : default_headers,
   } 
   if(method !=="GET"){
      default_menu.body = JSON.stringify(sendData)
   }
   
   let res = await fetch(url,default_menu);
   let temp = res;
   let status = await res.status;
   if(status && status===401 && isAuth){  // Refresh token for every auth request
      let message = await temp.json();
      if(message.name==='TokenExpiredError'){
         let genRes = await fetch(host+"generateRefreshToken",{
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Refresh_Token' : localStorage.getItem('refresh_token')
            },
         });
         let genStatus = await genRes.status;
         if(genStatus===202){
            let rsp = await genRes.json();
            let access_token = await rsp.accessToken;
            localStorage.setItem('access_token',access_token);
            default_menu.headers.Access_Token = access_token;
            return await fetch(url,default_menu);
         }
         return genRes;
      }
   }
   return res;
}

export const useConsoleLog = (value) => {  // for console.log() to state of component
   useEffect(()=>{
      console.log(value)
   },[value])
}

export const AsyncBtn = (props) => {
   let {pLoading,cLoading,className,pText,cText,cNormalText,disabled,forcedDisabled,...rest} = props
   if(typeof pLoading !== 'boolean') pLoading = false
   if(typeof cLoading !== 'boolean') cLoading = false
   if(typeof forcedDisabled !== 'boolean') forcedDisabled = false
   if(typeof pText !== 'string') pText = ''
   if(typeof cText !== 'string') cText = ''
   if(typeof cNormalText !== 'string') cNormalText = ''
   return(
      <button 
      className={`${className} ${(pLoading || cLoading || forcedDisabled)?' disabled':' '}`}
      disabled={pLoading || cLoading || forcedDisabled}
      {...rest}
      >
            {
               (pLoading)?
                  <span><span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> {pText}</span>:
               (cLoading)?
                  <span><span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> {cText}</span>:
               <span>{cNormalText}</span>
            }
      </button>
   )
}

export const useAlertSuccessDiv = (message) => {
   return message ? <div className="alert alert-success">{message}</div> : null
}

export const useAlertDangerDiv = (message) => {
   return message ? <div className="alert alert-danger" role="alert">{message}</div> : null
}

export const SortColumn = (props) => {
   // const {searchFilter={},Update_Object,column="",data={}} =  props ;
   // const {sortBy,sortType} = searchFilter;
   let {
         sortBy,
         sortType,
         column,
         columnLabel,
         data,
         handleSort
      } = props;
   if(typeof sortBy !== 'string') sortBy = 'updated_at'
   if(sortType !== 'asc') sortType = 'desc'
   if(typeof data !== 'object') data = {}
   if(typeof column !== 'string') column = ''
   if(typeof columnLabel !== 'string') columnLabel = ''
   const count = data.length;
   return <span>
       {columnLabel+" "}
       {(count>1)?
           (sortBy!== column)?<i className="fa fa-sort ptr" aria-hidden="true" onClick={()=>handleSort(column,'asc')}></i>:
           (sortBy === column && sortType==='asc' )?
           <i className="fa fa-sort-asc ptr" aria-hidden="true" onClick={()=>handleSort(column,'desc')}></i>
           :(sortBy ===column && sortType==='desc')?
           <i className="fa fa-sort-desc ptr" aria-hidden="true" onClick={()=>handleSort('updated_at','desc')}></i>
           :null
       :null
       }
   </span>
}
